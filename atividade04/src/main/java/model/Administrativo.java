package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tab_administrativo", schema = "public")
public class Administrativo extends Funcionario {

	@Column(name = "turno",nullable=false)
	private Integer turno;

	public Integer getTurno() {
		return turno;
	}

	public void setTurno(Integer turno) {
		this.turno = turno;
	}
	
	
}
