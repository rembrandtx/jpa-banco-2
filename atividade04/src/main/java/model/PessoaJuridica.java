package model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "tab_pessoajuridica", schema = "public"
//, uniqueConstraints = {@UniqueConstraint(name = "un_funcionario", columnNames = { "rg","rgOrgaoExpedidor","rgUf" }) }
)
public class PessoaJuridica {

	@Id
	@Column(name= "cnpj",length=11,nullable=false)
	private String cnpj;
	@Column(name= "nome",length=40,nullable=false)
	private String nome;
	@Column(name= "ramo_ativadade")
	private List<RamoAtividade> ramoAtivadade;
	@Column(name= "faturamento",nullable=false)
	private Double faturamento;
	@Column(name= "vendedores")
	private List<Vendedor> vendedores;
	
	
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<RamoAtividade> getRamoAtivadade() {
		return ramoAtivadade;
	}
	public void setRamoAtivadade(List<RamoAtividade> ramoAtivadade) {
		this.ramoAtivadade = ramoAtivadade;
	}
	public Double getFaturamento() {
		return faturamento;
	}
	public void setFaturamento(Double faturamento) {
		this.faturamento = faturamento;
	}
	public List<Vendedor> getVendedores() {
		return vendedores;
	}
	public void setVendedores(List<Vendedor> vendedores) {
		this.vendedores = vendedores;
	}
	
	
	
	
}
