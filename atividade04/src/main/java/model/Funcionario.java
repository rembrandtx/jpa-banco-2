package model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "tab_funcionarios", schema = "public", uniqueConstraints = {
@UniqueConstraint(name = "un_funcionario", columnNames = { "rg","rgOrgaoExpedidor","rgUf" }) })
public class Funcionario {

	@Id
	@Column(name= "cpf",length=11,nullable=false)
	private String cpf;
	@Column(name= "nome",length=40,nullable=false)
	private String nome;
	@Column(name= "rg",length=12,nullable=true)
	private String rg;
	@Column(name= "rg_Orgao_Expedidor",length=20,nullable=true)
	private String rgOrgaoExpedidor;
	@Column(name= "rg_uf",length=2,nullable=true)
	private String rgUf;
	@Column(name= "telefones",length=12)
	private List<String> telefones;
	@Column(name= "data_nascimento",nullable=false)
	private Date dataNascimento;
	@Column(name= "endereco",nullable=false)
	private Endereco endereco;
	
	
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getRgOrgaoExpedidor() {
		return rgOrgaoExpedidor;
	}
	public void setRgOrgaoExpedidor(String rgOrgaoExpedidor) {
		this.rgOrgaoExpedidor = rgOrgaoExpedidor;
	}
	public String getRgUf() {
		return rgUf;
	}
	public void setRgUf(String rgUf) {
		this.rgUf = rgUf;
	}
	public List<String> getTelefones() {
		return telefones;
	}
	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	
	
	
	
}
