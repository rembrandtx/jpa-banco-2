package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tab_cidade", schema = "public")
public class Cidade {
	
	@Column(name="sigla",length=3,nullable=false)
	private String sigla;
	@Column(name="nome",length=40,nullable=false)
	private String nome;
	@Column(name="estado",length=2,nullable=false)
	private Estado estado;
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
	
	
}
