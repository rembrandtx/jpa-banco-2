package model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "tab_vendedor", schema = "public"
//, uniqueConstraints = {@UniqueConstraint(name = "un_funcionario", columnNames = { "rg","rgOrgaoExpedidor","rgUf" }) }
		)
public class Vendedor extends Funcionario {

	@Column(name = "percentual_comissao",nullable=false)
	private Double percentualComissao;
	@Column(name = "situacao",length=3,nullable=false)
	private SituacaoVendedorEnum sitacao;
	@Column(name = "cliente")
	private List<PessoaJuridica> cliente;

	public Double getPercentualComissao() {
		return percentualComissao;
	}
	public void setPercentualComissao(Double percentualComissao) {
		this.percentualComissao = percentualComissao;
	}
	public SituacaoVendedorEnum getSitacao() {
		return sitacao;
	}
	public void setSitacao(SituacaoVendedorEnum sitacao) {
		this.sitacao = sitacao;
	}
	public List<PessoaJuridica> getCliente() {
		return cliente;
	}
	public void setCliente(List<PessoaJuridica> cliente) {
		this.cliente = cliente;
	}
	
	
	
}
