package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tab_ramoAtiviadade", schema = "public"
//, uniqueConstraints = {@UniqueConstraint(name = "un_funcionario", columnNames = { "rg","rgOrgaoExpedidor","rgUf" }) }
)
@SequenceGenerator(name="seq_ramo", sequenceName="sq_ramo")
public class RamoAtividade {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_ramo")
	@Column(name= "id",nullable=false)
	private Integer id;
	@Column(name="nome",length=40,nullable=false)
	private String nome;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	
	
	
}
