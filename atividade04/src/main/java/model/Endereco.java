package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "tab_endereco", schema = "public"
//, uniqueConstraints = {@UniqueConstraint(name = "un_funcionario", columnNames = { "rg","rgOrgaoExpedidor","rgUf" }) }
		)
public class Endereco {

	@Column(name= "logradouro",length=40, nullable=false)
	private String logradouro;
	@Column(name= "bairro",length=40, nullable=false)
	private String bairro;
	@Column(name= "cidade",length=3, nullable=false)
	private Cidade cidade;
	
	
	
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public Cidade getCidade() {
		return cidade;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	
	
}
