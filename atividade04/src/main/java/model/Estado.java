package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tab_estado", schema = "public")
public class Estado {

	@Column(name="sigla",length=2, nullable=false)
	private String sigla;
	@Column(name="nome",length=40, nullable=false)
	private String nome;
	
	
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
}
