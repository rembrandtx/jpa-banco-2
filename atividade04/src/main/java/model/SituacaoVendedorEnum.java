package model;

public enum SituacaoVendedorEnum {

	ATIVO ("ATV"), SUSPENSO ("SPN");

	private String situacao;

	private SituacaoVendedorEnum(String situacao) {
		this.situacao = situacao;
	}

	public String getSituacao() {
		return situacao;
	}
	
	
}
